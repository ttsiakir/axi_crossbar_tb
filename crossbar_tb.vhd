library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.wishbone_pkg.all;

library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;
use vunit_lib.bus_master_pkg.all;

library UNISIM;
use UNISIM.VCOMPONENTS.all;

entity crossbar_tb is
  generic (runner_cfg : string);
begin
end crossbar_tb;


architecture tb_arch of crossbar_tb is


        signal dl_AWVALID : std_logic;                     --! Write address valid signal
        signal dl_AWREADY : std_logic;                     --! Write address ready signal
        signal dl_AWADDR  : std_logic_vector(31 downto 0); --! Write address

        --! @name AXI Lite Write data channel
        signal dl_WVALID : std_logic;                     --! Write data valid signal
        signal dl_WREADY : std_logic;                     --! Write data ready signal
        signal dl_WDATA  : std_logic_vector(31 downto 0); --! Write data signal
        signal dl_WSTRB  : std_logic_vector(3 downto 0);  --! Write strobe. This is internally set to "1111"

        --! @name AXI Lite Write response channel
        signal dl_BVALID : std_logic;                    --! Write response valid signal
        signal dl_BREADY : std_logic;                    --! Write response ready signal
        signal dl_BRESP  : std_logic_vector(1 downto 0); --! Write response signals

        --! @name AXI Lite Read address channel
        signal dl_ARVALID : std_logic;                     --! Read address valid signal
        signal dl_ARREADY : std_logic;                     --! Read address ready signal
        signal dl_ARADDR  : std_logic_vector(31 downto 0); --! Read address signals

        --! @name AXI Lite Read data channel
        signal dl_RVALID : std_logic;                     --! Read data valid signal
        signal dl_RREADY : std_logic;                     --! Read data ready signal
        signal dl_RDATA  : std_logic_vector(31 downto 0); --! Read data signals
        signal dl_RRESP  : std_logic_vector(1 downto 0);  --! Read response signals. Unused in this IP



        signal gpio_AWVALID : std_logic;                     --! Write address valid signal
        signal gpio_AWREADY : std_logic;                     --! Write address ready signal
        signal gpio_AWADDR  : std_logic_vector(31 downto 0); --! Write address

        --! @name AXI Lite Write data channel
        signal gpio_WVALID : std_logic;                     --! Write data valid signal
        signal gpio_WREADY : std_logic;                     --! Write data ready signal
        signal gpio_WDATA  : std_logic_vector(31 downto 0); --! Write data signal
        signal gpio_WSTRB  : std_logic_vector(3 downto 0);  --! Write strobe. This is internally set to "1111"

        --! @name AXI Lite Write response channel
        signal gpio_BVALID : std_logic;                    --! Write response valid signal
        signal gpio_BREADY : std_logic;                    --! Write response ready signal
        signal gpio_BRESP  : std_logic_vector(1 downto 0); --! Write response signals

        --! @name AXI Lite Read address channel
        signal gpio_ARVALID : std_logic;                     --! Read address valid signal
        signal gpio_ARREADY : std_logic;                     --! Read address ready signal
        signal gpio_ARADDR  : std_logic_vector(31 downto 0); --! Read address signals

        --! @name AXI Lite Read data channel
        signal gpio_RVALID : std_logic;                     --! Read data valid signal
        signal gpio_RREADY : std_logic;                     --! Read data ready signal
        signal gpio_RDATA  : std_logic_vector(31 downto 0); --! Read data signals
        signal gpio_RRESP  : std_logic_vector(1 downto 0);  --! Read response signals. Unused in this IP


        signal AWVALID : std_logic;                     --! Write address valid signal
        signal AWREADY : std_logic;                     --! Write address ready signal
        signal AWADDR  : STD_LOGIC_VECTOR(31 downto 0); --! Write address

        --! @name AXI Lite Write data channel
        signal WVALID : std_logic;                     --! Write data valid signal
        signal WREADY : std_logic;                     --! Write data ready signal
        signal WDATA  : STD_LOGIC_VECTOR(31 downto 0); --! Write data signal
        signal WSTRB  : STD_LOGIC_VECTOR(3 downto 0);  --! Write strobe. This is internally set to "1111"

        --! @name AXI Lite Write response channel
        signal BVALID : std_logic;                    --! Write response valid signal
        signal BREADY : std_logic;                    --! Write response ready signal
        signal BRESP  : STD_LOGIC_VECTOR(1 downto 0); --! Write response signals

        --! @name AXI Lite Read address channel
        signal ARVALID : std_logic;                     --! Read address valid signal
        signal ARREADY : std_logic;                     --! Read address ready signal
        signal ARADDR  : STD_LOGIC_VECTOR(31 downto 0); --! Read address signals

        --! @name AXI Lite Read data channel
        signal RVALID : std_logic;                     --! Read data valid signal
        signal RREADY : std_logic;                     --! Read data ready signal
        signal RDATA  : STD_LOGIC_VECTOR(31 downto 0); --! Read data signals
        signal RRESP  : STD_LOGIC_VECTOR(1 downto 0);  --! Read response signals. Unused in this IP

        signal m_axi_awaddr  : STD_LOGIC_VECTOR ( 63 downto 0 );
        signal m_axi_awprot  : STD_LOGIC_VECTOR ( 5 downto 0 );
        signal m_axi_awvalid : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_awready : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_wdata   : STD_LOGIC_VECTOR ( 63 downto 0 );
        signal m_axi_wstrb   : STD_LOGIC_VECTOR ( 7 downto 0 );
        signal m_axi_wvalid  : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_wready  : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_bresp   : STD_LOGIC_VECTOR ( 3 downto 0 );
        signal m_axi_bvalid  : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_bready  : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_araddr  : STD_LOGIC_VECTOR ( 63 downto 0 );
        signal m_axi_arprot  : STD_LOGIC_VECTOR ( 5 downto 0 );
        signal m_axi_arvalid : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_arready : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_rdata   : STD_LOGIC_VECTOR ( 63 downto 0 );
        signal m_axi_rresp   : STD_LOGIC_VECTOR ( 3 downto 0 );
        signal m_axi_rvalid  : STD_LOGIC_VECTOR ( 1 downto 0 );
        signal m_axi_rready  : STD_LOGIC_VECTOR ( 1 downto 0 );

        signal dl_awprot   : std_logic_vector(2 downto 0);
        signal gpio_awprot : std_logic_vector(2 downto 0);
        signal dl_arprot   : std_logic_vector(2 downto 0);
        signal gpio_arprot : std_logic_vector(2 downto 0);

        signal gpio_out : std_logic_vector(31 downto 0);


        signal clk_i : std_logic;
        signal reset_n : std_logic;

        signal delay_i : std_logic;
        signal delay_o : std_logic;

        COMPONENT axi_crossbar_0
  PORT (
    aclk          : IN  STD_LOGIC;
    aresetn       : IN  STD_LOGIC;
    s_axi_awaddr  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_awprot  : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_awvalid : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_awready : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_wdata   : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_wstrb   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_wvalid  : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_wready  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_bresp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_bvalid  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_bready  : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_araddr  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_arprot  : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_arvalid : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_arready : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_rdata   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_rresp   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_rvalid  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    s_axi_rready  : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axi_awaddr  : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axi_awprot  : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    m_axi_awvalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_awready : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_wdata   : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axi_wstrb   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axi_wvalid  : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_wready  : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_bresp   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_bvalid  : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_bready  : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_araddr  : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axi_arprot  : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    m_axi_arvalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_arready : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_rdata   : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axi_rresp   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_rvalid  : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_rready  : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
  );
END COMPONENT;

  constant c_bus_handle : bus_master_t := new_bus(data_length => 32, address_length => 32);

begin


        gpio_AWADDR <= m_axi_awaddr(31 downto 0);
        ----gpio_awprot  <= (others => '0');
        gpio_AWVALID <= m_axi_awvalid(0);
        --m_axi_awready(0) <= gpio_AWREADY;
        gpio_WDATA <=  m_axi_wdata(31 downto 0)  ;
        --m_axi_wstrb   <= (others => '1');
        gpio_WVALID <= m_axi_wvalid(0);
        m_axi_wready(0)<='1';
        --m_axi_bresp   <= (others => '0');
        --m_axi_bvalid(0)  <= gpio_BVALID;
        --gpio_BREADY <= m_axi_bready(0);
        gpio_ARADDR <= m_axi_araddr(31 downto 0);
        ----m_axi_arprot <= (gpio_arprot);
        gpio_ARVALID <= m_axi_arvalid(0);
        m_axi_arready(0) <= gpio_ARREADY;
        --m_axi_rdata(31 downto 0)   <= gpio_RDATA;
        --m_axi_rresp   <= (others => '0');
        --m_axi_rvalid(0)  <= gpio_RVALID;
        --gpio_RREADY <= m_axi_rready(0);
--
        dl_AWADDR <= m_axi_awaddr(63 downto 32);
        ----dl_awprot
        dl_AWVALID <= m_axi_awvalid(1);
        --m_axi_awready(1) <= dl_AWREADY;
        dl_WDATA <= m_axi_wdata(63 downto 32);
        dl_WVALID <= m_axi_wvalid(1);
        m_axi_wready(1)  <= '1';
        --m_axi_bvalid(1)  <= dl_BVALID;
        --dl_BREADY <= m_axi_bready(1);
        dl_ARADDR <= m_axi_araddr(63 downto 32);
        ----m_axi_arprot <= dl_arprot;
        dl_ARVALID <= m_axi_arvalid(1);
        m_axi_arready(1) <= dl_ARREADY;
        --m_axi_rdata(63 downto 32)   <= dl_RDATA;
        --m_axi_rvalid(1)  <= dl_RVALID;
        --dl_RREADY <= m_axi_rready(1);

  axi_delay_line_prog_1 : entity work.axi_delay_line_prog
    generic map (
      g_axi_clk_freq       => 25000000,
      g_dly_clk_freq       => 25000000/5,
      g_use_ext_clock      => false,
      g_max_delay          => 8,
      g_use_true_shift_reg => true
    )
    port map (
      axi_aresetn_i => reset_n,
      axi_clk_i     => clk_i,
      axi_awvalid_i => dl_awvalid,
      axi_awready_o => dl_awready,
      axi_awaddr_i  => dl_awaddr(3 downto 0),
      axi_awprot_i  => dl_awprot,
      axi_wvalid_i  => dl_wvalid,
      axi_wready_o  => dl_wready,
      axi_wdata_i   => dl_wdata,
      axi_wstrb_i   => dl_wstrb,
      axi_bvalid_o  => dl_bvalid,
      axi_bready_i  => dl_bready,
      axi_bresp_o   => dl_bresp,
      axi_arvalid_i => dl_arvalid,
      axi_arready_o => dl_arready,
      axi_araddr_i  => dl_araddr(3 downto 0),
      axi_arprot_i  => dl_arprot,
      axi_rvalid_o  => dl_rvalid,
      axi_rready_i  => dl_rready,
      axi_rdata_o   => dl_rdata,
      axi_rresp_o   => dl_rresp,
      ext_clk_i     => '0',
      delay_i       => delay_i,
      delay_o       => delay_o
    ); 
    
        GPIO : entity work.axi_in_out
            generic map (
                with_builtin_sync_g      => true,
                with_builtin_tristates_g => false
            )
            port map(
                axi_clk_i      => clk_i,
                axi_areset_n_i => reset_n,
                axi_awvalid_i  => gpio_AWVALID,
                axi_awready_o  => gpio_awready,
                axi_awaddr_i   => gpio_awaddr(3 downto 0),
                axi_awprot_i   => gpio_awprot,
                axi_wvalid_i   => gpio_wvalid,
                axi_wready_o   => gpio_wready,
                axi_wdata_i    => gpio_wdata,
                axi_wstrb_i    => gpio_wstrb,
                axi_bvalid_o   => gpio_bvalid,
                axi_bready_i   => gpio_bready,
                axi_bresp_o    => gpio_bresp,
                axi_arvalid_i  => gpio_arvalid,
                axi_arready_o  => gpio_arready,
                axi_araddr_i   => gpio_araddr(3 downto 0),
                axi_arprot_i   => gpio_arprot,
                axi_rvalid_o   => gpio_rvalid,
                axi_rready_i   => gpio_rready,
                axi_rdata_o    => gpio_rdata,
                axi_rresp_o    => gpio_rresp,

                io_inout_io => open,
                io_in_i     => (others => '0'),
                io_out_o    => gpio_out
            );


        x_vunit_axi_lite_master : entity vunit_lib.axi_lite_master
        generic map (
          bus_handle => c_bus_handle
        )
        port map (
          aclk    => clk_i,
          arready => ARREADY,
          arvalid => ARVALID,
          araddr  => ARADDR,
          rready  => RREADY,
          rvalid  => RVALID,
          rdata   => RDATA,
          rresp   => RRESP,
          awready => AWREADY,
          awvalid => AWVALID,
          awaddr  => AWADDR,
          wready  => WREADY,
          wvalid  => WVALID,
          wdata   => WDATA,
          wstrb   => WSTRB,
          bvalid  => BVALID,
          bready  => BREADY,
          bresp   => (others => '0')
        );


        crossbar_gen : axi_crossbar_0
          PORT MAP (
            aclk             => clk_i,
            aresetn          => reset_n,
            s_axi_awaddr     => AWADDR,
            s_axi_awprot     => (others => '0'),
            s_axi_awvalid(0) => AWVALID,
            s_axi_awready(0) => AWREADY,
            s_axi_wdata      => WDATA,
            s_axi_wstrb      => WSTRB,
            s_axi_wvalid(0)  => WVALID,
            s_axi_wready(0)  => WREADY,
            s_axi_bresp      => BRESP,
            s_axi_bvalid(0)  => BVALID,
            s_axi_bready(0)  => BREADY,
            s_axi_araddr     => ARADDR,
            s_axi_arprot     => (others => '0'),
            s_axi_arvalid(0) => ARVALID,
            s_axi_arready(0) => ARREADY,
            s_axi_rdata      => RDATA,
            s_axi_rresp      => RRESP,
            s_axi_rvalid(0)  => RVALID,
            s_axi_rready(0)  => RREADY,
            m_axi_awaddr     => m_axi_awaddr,
            m_axi_awprot     => m_axi_awprot,
            m_axi_awvalid    => m_axi_awvalid,
            m_axi_awready    => m_axi_awready,
            m_axi_wdata      => m_axi_wdata,
            m_axi_wstrb      => m_axi_wstrb,
            m_axi_wvalid     => m_axi_wvalid,
            m_axi_wready     => m_axi_wready,
            m_axi_bresp      => m_axi_bresp,
            m_axi_bvalid     => m_axi_bvalid,
            m_axi_bready     => m_axi_bready,
            m_axi_araddr     => m_axi_araddr,
            m_axi_arprot     => m_axi_arprot,
            m_axi_arvalid    => m_axi_arvalid,
            m_axi_arready    => m_axi_arready,
            m_axi_rdata      => m_axi_rdata,
            m_axi_rresp      => m_axi_rresp,
            m_axi_rvalid     => m_axi_rvalid,
            m_axi_rready     => m_axi_rready
          );

            clk_proc: process
            begin
            
              clk_i <= '1';
              wait for 10 ns;
              clk_i <= '0';
              wait for 10 ns;
            
            end process;
            
            main : process
            begin
              test_runner_setup(runner, runner_cfg);
            
                reset_n <= '0';
                wait for 10 ns;
                reset_n <= '1';
            
                wait for 1 ns;
            
                if run("test_crossbar") then
            
                info("Configure DAC interface to MAX5719 parameters");

                for i in 0 to 15 loop
            
                  write_bus(net, c_bus_handle, 8, std_logic_vector(to_unsigned(i,32)));
                  wait_until_idle(net,c_bus_handle);

                end loop;
            
                end if;
            
            
              wait for 10 ns;
              test_runner_cleanup(runner);
            end process;



end architecture tb_arch;