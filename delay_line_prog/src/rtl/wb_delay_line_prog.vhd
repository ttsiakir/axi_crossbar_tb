-------------------------------------------------------------------------------
--! @file
--! @author Adrian Byszuk <adrian.byszuk@cern.ch> (CERN TE-EPC-CCE)
--! @date 2020-11-11
--! @brief Dynamic delay line with Wishbone interface
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.wishbone_pkg.all;

--! @brief Programmable delay line.
--! @details
--! Control and status available through Wishbone bus.
--! Register documentation available in the IP directory.
--! @section regmap Register memory map
--! @htmlinclude [delay_line_regs.html]
entity wb_delay_line_prog is
  generic (
    --! Wishbone clock rate
    g_wb_clk_freq : positive;
    --! Delay line clock rate
    g_dly_clk_freq : positive;
    --! Use external clock for delay line. If false uses internal prescaler
    g_use_ext_clock : boolean;
    --! Max delay
    g_max_delay : positive range 1 to 65535;
    --! Use embedded SLR primitives (lower logic consumption) or flip-flops
    g_use_true_shift_reg : boolean := true
  );
  port (
    --! @name Wishbone interface
    wb_rst_i : in std_ulogic; --! Wishbone reset
    wb_clk_i : in std_ulogic; --! Wishbone clock
    wb_i     : in t_wishbone_slave_in; --! Wishbone input ports
    wb_o     : out t_wishbone_slave_out; --! Wishbone output ports

    --! @name External ports
    ext_clk_i : in std_ulogic; --! External clock for delay line (optional)
    delay_i   : in std_ulogic; --! Signal input
    delay_o   : out std_ulogic --! Delayed signal output
  );
end entity wb_delay_line_prog;

architecture rtl of wb_delay_line_prog is

  function to_sl(a : boolean) return std_ulogic is
  begin
    if a then
      return '1';
    else
      return '0';
    end if;
  end function;

  constant c_clock_freq : std_logic_vector(31 downto 0) :=
    std_logic_vector(to_unsigned(g_dly_clk_freq, 32));

  signal enable      : std_ulogic;
  signal clear       : std_ulogic;
  signal delay_ticks : std_logic_vector(15 downto 0);

  signal delay_clk   : std_logic;
  signal clk_en      : std_logic;
  signal clk_en_prev : std_logic := '0'; -- used as edge detector
  signal delay_line  : std_logic_vector(g_max_delay-1 downto 0) := (others => '0');
  signal delay_out   : std_logic := '0';
  -- clock enable for delay line, used in internal clock mode with slow clock
  -- edge detector, directly driven by register external clock mode
  signal delay_clken : std_logic := '0';

begin

  gen_ext_clk : if g_use_ext_clock generate

    -- assume that external clock is a true clock with proper clock constraints
    -- in such case, we can wire up edge detect to "always rising"
    delay_clk   <= ext_clk_i;
    delay_clken <= enable;

  end generate gen_ext_clk;

  gen_int_clk : if not g_use_ext_clock generate

    assert g_wb_clk_freq > 2 * g_dly_clk_freq
    report "When using internal delay line clock, target frequency must be at"
            & " least 2x smaller than Wishbone clock";

    -- Internal clock comes from clock divider done in logic.
    -- Using it to drive long chain of shift reg may create timing problems
    -- In that case switch to "detect edge and clock enable" scheme
    delay_clk <= wb_clk_i;

    --! this divider creates pulse driving the shift register
    x_clk_div : entity work.clock_divider
    generic map(
      ticks_g        => g_wb_clk_freq / g_dly_clk_freq,
      reset_active_g => '0'
    )
    port map(
      clk_i    => wb_clk_i,
      rst_i    => enable,
      clk_en_o => clk_en
    );

    --! Generates a clock enable pulse on rising clk edge of target clock
    p_edge : process(wb_clk_i)
    begin
      if rising_edge(wb_clk_i) then
        if wb_rst_i = '1' then
          clk_en_prev <= '0';
          delay_clken <= '0';
        else
          -- detect rising edge from clock divider, but also check control register
          clk_en_prev <= clk_en;
          delay_clken <= not(clk_en_prev) and clk_en and enable;
        end if;
      end if;
    end process;

  end generate gen_int_clk;

  --! Control/status register block
  x_regs : entity work.delay_line_regs
  port map (
    rst_n_i => not wb_rst_i,
    clk_i   => wb_clk_i,
    wb_i    => wb_i,
    wb_o    => wb_o,
    --
    ctrl_enable_o               => enable,
    ctrl_clear_o                => clear,
    dly_ticks_o                 => delay_ticks,
    status_use_true_shift_reg_i => to_sl(g_use_true_shift_reg),
    status_max_delay_i          => std_logic_vector(to_unsigned(g_max_delay, 16)),
    clock_freq_i                => c_clock_freq
  );

  --! Delay line based on true shift register primitives (SLR16, SLR32 etc.)
  --! Cannot be reset, old data must be shifted out
  gen_slr: if g_use_true_shift_reg generate

    p_dly : process(delay_clk)
    begin
      if rising_edge(delay_clk) then
        if delay_clken = '1' then
          delay_line <= delay_line(delay_line'high-1 downto 0) & delay_i;
        end if;
      end if;
    end process;

  end generate gen_slr;

  --! Delay line based on generic flip flops
  --! This implementation will consume more resources, but can be reset
  gen_ff: if not g_use_true_shift_reg generate

    p_dly : process(delay_clk)
    begin
      if rising_edge(delay_clk) then
        if clear = '1' then
          delay_line <= (others => '0');
        else
          if delay_clken = '1' then
            delay_line <= delay_line(delay_line'high-1 downto 0) & delay_i;
          end if;
        end if;
      end if;
    end process;

  end generate gen_ff;

  delay_out <= delay_line(to_integer(unsigned(delay_ticks)));
  delay_o   <= delay_out;

end architecture rtl;
