-------------------------------------------------------------------------------
--! @file
--! @author Adrian Byszuk <adrian.byszuk@cern.ch> (CERN TE-EPC-CCE)
--! @date 2022-08-22
--! @brief Dynamic delay line with AXI interface
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

--! @brief Programmable delay line.
--! @details
--! Control and status available through Wishbone bus.
--! Register documentation available in the IP directory.
--! @section regmap Register memory map
--! @htmlinclude [delay_line_regs.html]
entity axi_delay_line_prog is
  generic (
    --! Wishbone clock rate
    g_axi_clk_freq : positive;
    --! Delay line clock rate
    g_dly_clk_freq : positive;
    --! Use external clock for delay line. If false uses internal prescaler
    g_use_ext_clock : boolean;
    --! Max delay
    g_max_delay : positive range 1 to 65535;
    --! Use embedded SLR primitives (lower logic consumption) or flip-flops
    g_use_true_shift_reg : boolean := true
  );
  port (
    --! @name AXI Lite clock and reset
    axi_aresetn_i : in std_logic; --! AXI reset
    axi_clk_i     : in std_logic; --! AXI clock

    --! @name AXI write address (write channel)
    axi_awvalid_i : in  std_logic; --! AXI write address valid
    axi_awready_o : out std_logic; --! AXI wrire address ready
    axi_awaddr_i  : in  std_logic_vector(3 downto 0); --! AXI write address
    axi_awprot_i  : in  std_logic_vector(2 downto 0); --! AXI write protection encoding (not used)

    --! @name AXI write data (write channel)
    axi_wvalid_i : in  std_logic; --! AXI write valid
    axi_wready_o : out std_logic; --! AXI write ready
    axi_wdata_i  : in  std_logic_vector(31 downto 0); --! AXI write data
    axi_wstrb_i  : in  std_logic_vector(3 downto 0); --!  AXI write strobe (not used)
    --
    --! @name AXI address write response (not used)
    axi_bvalid_o : out std_logic; --! AXI write response valid (not used)
    axi_bready_i : in  std_logic; --! AXI write response ready (not used)
    axi_bresp_o  : out std_logic_vector(1 downto 0); --! AXI read response (not used)

    --! @name AXI address read (write channel)
    axi_arvalid_i : in  std_logic; --! AXI read address valid
    axi_arready_o : out std_logic; --! AXI read address ready
    axi_araddr_i  : in  std_logic_vector(3 downto 0); --! AXI read address
    axi_arprot_i  : in  std_logic_vector(2 downto 0); --! AXI read address protection encoding (not used)

    --! @name AXI data read (read channel)
    axi_rvalid_o : out std_logic; --! AXI read data valid
    axi_rready_i : in  std_logic; --! AXI read data ready
    axi_rdata_o  : out std_logic_vector(31 downto 0); --! AXI read data
    axi_rresp_o  : out std_logic_vector(1 downto 0); --! AXI read response (not used)

    --! @name External ports
    ext_clk_i : in std_ulogic; --! External clock for delay line (optional)
    delay_i   : in std_ulogic; --! Signal input
    delay_o   : out std_ulogic --! Delayed signal output
  );
end entity axi_delay_line_prog;

architecture rtl of axi_delay_line_prog is

  function to_sl(a : boolean) return std_ulogic is
  begin
    if a then
      return '1';
    else
      return '0';
    end if;
  end function;

  constant c_clock_freq : std_logic_vector(31 downto 0) :=
    std_logic_vector(to_unsigned(g_dly_clk_freq, 32));

  signal enable           : std_ulogic;
  signal clear            : std_ulogic;
  signal clear_sync       : std_ulogic;
  signal delay_ticks      : std_logic_vector(15 downto 0);
  signal delay_ticks_sync : std_logic_vector(delay_ticks'range);
  signal ctrl_wr_stb      : std_ulogic;
  signal delay_ticks_stb  : std_ulogic;

  signal delay_clk  : std_logic;
  signal delay_line : std_logic_vector(g_max_delay-1 downto 0) := (others => '0');
  signal delay_out  : std_logic := '0';
  -- clock enable for delay line, used in internal clock mode with slow clock
  -- edge detector, directly driven by register external clock mode
  signal delay_clken : std_logic := '0';

begin

  gen_ext_clk : if g_use_ext_clock generate
    signal rst_esync_n : std_logic;
    signal ctrl_in     : std_logic_vector(1 downto 0);
    signal ctrl_out    : std_logic_vector(ctrl_in'range);
  begin
    -- assume that external clock is a true clock with proper clock constraints
    -- in such case, we can wire up edge detect to "always rising"
    delay_clk <= ext_clk_i;
    ctrl_in   <= (enable, clear);

    (delay_clken, clear_sync) <= ctrl_out;

    x_rst_sync : entity work.gc_reset_multi_aasd
    generic map (
      g_CLOCKS  => 1,
      g_RST_LEN => 1
    )
    port map (
      arst_i     => not axi_aresetn_i,
      clks_i(0)  => ext_clk_i,
      rst_n_o(0) => rst_esync_n
    );

    x_ctrl_sync : entity work.gc_sync_word_wr
    generic map (
      g_AUTO_WR => false,
      g_WIDTH   => ctrl_in'length
    )
    port map (
      clk_in_i    => axi_clk_i,
      rst_in_n_i  => axi_aresetn_i,
      clk_out_i   => ext_clk_i,
      rst_out_n_i => rst_esync_n,

      data_i => ctrl_in,
      wr_i   => ctrl_wr_stb,
      busy_o => open,
      ack_o  => open,
      data_o => ctrl_out,
      wr_o   => open
    );

    x_dly_sync : entity work.gc_sync_word_wr
    generic map (
      g_AUTO_WR => false,
      g_WIDTH   => delay_ticks'length
    )
    port map (
      clk_in_i    => axi_clk_i,
      rst_in_n_i  => axi_aresetn_i,
      clk_out_i   => ext_clk_i,
      rst_out_n_i => rst_esync_n,

      data_i => delay_ticks,
      wr_i   => delay_ticks_stb,
      busy_o => open,
      ack_o  => open,
      data_o => delay_ticks_sync,
      wr_o   => open
    );
  end generate gen_ext_clk;

  gen_int_clk : if not g_use_ext_clock generate
    signal clk_en      : std_logic;
    signal clk_en_prev : std_logic := '0'; -- used as edge detector
  begin

    assert g_axi_clk_freq > 2 * g_dly_clk_freq
    report "When using internal delay line clock, target frequency must be at"
            & " least 2x smaller than AXI clock";

    -- Internal clock comes from clock divider done in logic.
    -- Using it to drive long chain of shift reg may create timing problems
    -- In that case switch to "detect edge and clock enable" scheme
    delay_clk        <= axi_clk_i;
    clear_sync       <= clear;
    delay_ticks_sync <= delay_ticks;

    --! this divider creates pulse driving the shift register
    x_clk_div : entity work.clock_divider
    generic map(
      ticks_g        => g_axi_clk_freq / g_dly_clk_freq,
      reset_active_g => '0'
    )
    port map(
      clk_i    => axi_clk_i,
      rst_i    => enable,
      clk_en_o => clk_en
    );

    --! Generates a clock enable pulse on rising clk edge of target clock
    p_edge : process(axi_clk_i)
    begin
      if rising_edge(axi_clk_i) then
        if axi_aresetn_i = '0' then
          clk_en_prev <= '0';
          delay_clken <= '0';
        else
          -- detect rising edge from clock divider, but also check control register
          clk_en_prev <= clk_en;
          delay_clken <= not(clk_en_prev) and clk_en and enable;
        end if;
      end if;
    end process;

  end generate gen_int_clk;

  --! Control/status register block
  x_regs : entity work.delay_line_regs_axi
  port map (
    aclk     => axi_clk_i,
    areset_n => axi_aresetn_i,
    awvalid  => axi_awvalid_i,
    awready  => axi_awready_o,
    awaddr   => axi_awaddr_i,
    awprot   => axi_awprot_i,
    wvalid   => axi_wvalid_i,
    wready   => axi_wready_o,
    wdata    => axi_wdata_i,
    wstrb    => axi_wstrb_i,
    bvalid   => axi_bvalid_o,
    bready   => axi_bready_i,
    bresp    => axi_bresp_o,
    arvalid  => axi_arvalid_i,
    arready  => axi_arready_o,
    araddr   => axi_araddr_i,
    arprot   => axi_arprot_i,
    rvalid   => axi_rvalid_o,
    rready   => axi_rready_i,
    rdata    => axi_rdata_o,
    rresp    => axi_rresp_o,
    --
    ctrl_enable_o               => enable,
    ctrl_clear_o                => clear,
    ctrl_wr_o                   => ctrl_wr_stb,
    dly_ticks_o                 => delay_ticks,
    dly_ticks_wr_o              => delay_ticks_stb,
    status_use_true_shift_reg_i => to_sl(g_use_true_shift_reg),
    status_max_delay_i          => std_logic_vector(to_unsigned(g_max_delay, 16)),
    clock_freq_i                => c_clock_freq
  );

  --! Delay line based on true shift register primitives (SLR16, SLR32 etc.)
  --! Cannot be reset, old data must be shifted out
  gen_slr: if g_use_true_shift_reg generate

    p_dly : process(delay_clk)
    begin
      if rising_edge(delay_clk) then
        if delay_clken = '1' then
          delay_line <= delay_line(delay_line'high-1 downto 0) & delay_i;
        end if;
      end if;
    end process;

  end generate gen_slr;

  --! Delay line based on generic flip flops
  --! This implementation will consume more resources, but can be reset
  gen_ff: if not g_use_true_shift_reg generate

    p_dly : process(delay_clk)
    begin
      if rising_edge(delay_clk) then
        if clear_sync = '1' then
          delay_line <= (others => '0');
        else
          if delay_clken = '1' then
            delay_line <= delay_line(delay_line'high-1 downto 0) & delay_i;
          end if;
        end if;
      end if;
    end process;

  end generate gen_ff;

  delay_out <= delay_line(to_integer(unsigned(delay_ticks_sync)));
  delay_o   <= delay_out;

end architecture rtl;
