"""Convenience methods for controlling delay_line_prog module."""

import delay_line_regs_const as const


class delay_line_prog:
    """
    Provides access to all module functionality and registers.

    Also implements custom methods for most used functionality.
    """

    def __init__(self, bus, offset):
        """Class constructor.

        :param bus: Communication object providing read() and write() methods
        :param offset: IPs base address in memory map
        """
        self._bus = bus
        self._offset = offset
        # These 3 values can be obtained from modules status register
        self.freq = 0
        self.max_delay = 0
        self.uses_true_shift_reg = False

    def read(self, addr):
        """Read one of module registers.

        :param addr: Register relative address
        :return: Data read from register
        """
        assert addr <= const.DELAY_LINE_REGS_SIZE
        return self._bus.read(self._offset + addr)

    def write(self, addr, data):
        """Write to one of module registers.

        :param addr: Register relative address
        :param data: Register data to be written
        """
        assert addr <= const.DELAY_LINE_REGS_SIZE
        self._bus.write(self._offset + addr, data)

    def get_params(self):
        """Get module params configured at synthesis stage.

        Should be called after object creation to fetch IP status.
        """
        status = self.read(const.ADDR_DELAY_LINE_REGS_STATUS)
        self.max_delay = ((status & const.DELAY_LINE_REGS_STATUS_MAX_DELAY)
                          >> const.DELAY_LINE_REGS_STATUS_MAX_DELAY_OFFSET)
        if status & const.DELAY_LINE_REGS_STATUS_USE_TRUE_SHIFT_REG:
            self.uses_true_shift_reg = True
        else:
            self.uses_true_shift_reg = False

        self.freq = self.read(const.ADDR_DELAY_LINE_REGS_CLOCK_FREQ)

    def clear(self):
        """Clear the delay line.

        This feature is available only if hardware doesn't use true shift reg
        primitives.
        """
        if self.uses_true_shift_reg:
            raise Exception("It's impossible to clear true shift reg")
        else:
            self.write(const.ADDR_DELAY_LINE_REGS_CTRL,
                       const.DELAY_LINE_REGS_CTRL_CLEAR)

    def enable(self, state):
        """Enable or disable the delay line.

        :param state: Target state - True to enable, False to disable
        """
        if state:
            val = const.DELAY_LINE_REGS_CTRL_ENABLE
        else:
            val = 0x0
        self.write(const.ADDR_DELAY_LINE_REGS_CTRL, val)

    def set_delay(self, steps):
        """Set target delay, declared as a number of delay steps.

        Target delay time will depend on the clock period of delay line through
        a simple formula: time = (1/freq) * steps)
        :param steps: Number of delay steps. Can't be higher than max delay.
        """
        assert steps <= self.max_delay

        self.write(const.ADDR_DELAY_LINE_REGS_DLY_TICKS, steps)
