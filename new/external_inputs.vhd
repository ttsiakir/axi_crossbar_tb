-------------------------------------------------------------------------------
--! @file external_inputs.vhd
--! @author Johannes Walter <johannes.walter@cern.ch>
--! @copyright CERN TE-EPC-CCE
--! @date 2014-04-02
--! @brief Synchronize and filter external inputs
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

--! @brief Synchronize and filter external inputs
entity external_inputs is
    generic (
        --! Initial value of input signals
        init_value_g : std_ulogic := '0';
        --! Number of inputs
        num_inputs_g : positive := 1;
        --! Add glitch filter
        filter_g : boolean := true;
        --! Glitch filter length (max glitch width)
        filter_len_g : positive := 1
      );
    port (
        --! @name Clock, resets and control
        clk_i       : in std_ulogic; --! Main clock
        rst_asy_n_i : in std_ulogic; --! Asynchronous reset (exclusive with sync reset)
        rst_syn_i   : in std_ulogic; --! Synchronous reset (exclusive with async reset)
        filter_en_i : in std_ulogic := '1'; --! Clock enable for glitch filter, always enabled by default

        --! Input signals
        sig_i : in  std_ulogic_vector(num_inputs_g - 1 downto 0);

        --! Synchronized and filtered output signals
        sig_o : out std_ulogic_vector(num_inputs_g - 1 downto 0));
end entity external_inputs;

architecture rtl of external_inputs is

    ---------------------------------------------------------------------------
    -- Internal Wires
    ---------------------------------------------------------------------------

    signal sig : std_ulogic_vector(sig_i'range);

begin -- architecture rtl

    ---------------------------------------------------------------------------
    -- Instances
    ---------------------------------------------------------------------------

    inst_gen : for i in sig_i'range generate
        -- Synchronize the inputs into the local clock domain
        delay_inst : entity work.delay
            generic map (
                init_value_g => init_value_g,
                num_delay_g  => 2)
            port map (
                clk_i       => clk_i,
                rst_asy_n_i => rst_asy_n_i,
                rst_syn_i   => rst_syn_i,
                en_i        => '1',
                sig_i       => sig_i(i),
                dlyd_o      => sig(i));

        -- Direct output without glitch filter
        no_filter_gen : if filter_g = false generate
            sig_o(i) <= sig(i);
        end generate no_filter_gen;

        -- Filter glitches
        filter_gen : if filter_g = true generate
            glitch_filter_inst : entity work.glitch_filter
                generic map (
                    init_value_g => init_value_g,
                    num_delay_g  => filter_len_g)
                port map (
                    clk_i       => clk_i,
                    rst_asy_n_i => rst_asy_n_i,
                    rst_syn_i   => rst_syn_i,
                    en_i        => filter_en_i,
                    sig_i       => sig(i),
                    sig_o       => sig_o(i));
        end generate filter_gen;
    end generate inst_gen;

end architecture rtl;
