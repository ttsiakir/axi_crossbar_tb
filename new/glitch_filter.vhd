-------------------------------------------------------------------------------
--! @file glitch_filter.vhd
--! @author Johannes Walter <johannes.walter@cern.ch>
--! @copyright CERN TE-EPC-CCE
--! @date 2014-04-02
--! @brief Filter glitches with an N-stage shift register.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

--! @brief Filter glitches with an N-stage shift register.
entity glitch_filter is
  generic (
    --! Initial value of input signal
    init_value_g : std_ulogic := '0';
    --! Number of delay stages
    num_delay_g : positive := 1);
  port (
    --! @name Clock and resets
    clk_i       : in std_ulogic;
    rst_asy_n_i : in std_ulogic;
    rst_syn_i   : in std_ulogic;

    --! Enable
    en_i : in std_ulogic;
    --! Input signal (must be synchronous to main clock)
    sig_i : in  std_ulogic;
    --! Filtered output signal
    sig_o : out std_ulogic);
end entity glitch_filter;

--! RTL implementation of glitch_filter
architecture rtl of glitch_filter is

  constant all_ones  : std_ulogic_vector(num_delay_g downto 0) := (others => '1');
  constant all_zeros : std_ulogic_vector(num_delay_g downto 0) := (others => '0');
  ---------------------------------------------------------------------------
  -- Internal Registers
  ---------------------------------------------------------------------------
  signal dlyd_sig : std_ulogic_vector(num_delay_g - 1 downto 0) := (others => init_value_g);

begin

  --! Filter signal
  regs : process (clk_i, rst_asy_n_i) is
  begin -- process regs
    -- Other modules use internal reset procedure here, but for some reason
    -- this may trigger a segfault with (at least) Modelsim 10.7a, so let's
    -- fall back to more classic and verbose style
    if rst_asy_n_i = '0' then
      dlyd_sig <= (others => init_value_g);
      sig_o    <= init_value_g;
    elsif rising_edge(clk_i) then
      if rst_syn_i = '1' then
        dlyd_sig <= (others => init_value_g);
        sig_o    <= init_value_g;
      else
        if en_i = '1' then
          dlyd_sig <= dlyd_sig(dlyd_sig'high - 1 downto 0) & sig_i;

          if (dlyd_sig & sig_i) = all_ones then
            sig_o <= '1';
          elsif (dlyd_sig & sig_i) = all_zeros then
            sig_o <= '0';
          end if;
        end if;
      end if;
    end if;
  end process regs;

end architecture rtl;
