-------------------------------------------------------------------------------
-- File       : clock_divider.vhd
-- Author     : Benjamin Todd <benjamin.todd@cern.ch>
-- Company    : CERN TE-EPC-CCE
-- Created    : 2014-05-12
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: simple clock division
-------------------------------------------------------------------------------
--Revision List
--Version Author  Date		   Changes
--
--0.1     BMT     12.05.2014  New Version
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock_divider is
  generic (
    ticks_g : natural	:= 40;-- number of ticks between output pulses
    reset_active_g : std_logic	:= '0'			-- polarity of the reset
  );
  port (
    --Global Control Signals
    clk_i : in std_logic;
    rst_i : in std_logic;
    --Outputs
    clk_en_o : out std_logic
	);
end clock_divider;

architecture rtl of clock_divider is

signal clk, rst : std_logic; -- internal copies of external signals
signal ctr : natural range 0 to ticks_g - 1;

begin

assert ticks_g mod 2 = 0 -- check that ticks is set to a natural number 
report "clock_divider: clock division is not 2**N, this creates a sub-optimum circuit" 
severity warning;

assert ticks_g /= 0 -- check that ticks is not set to zero
report "clock_divider: clock division is set to zero" 
severity error;

clk <= clk_i;
rst <= rst_i;

-- free running down counter from ticks-1 to zero 
count_p : process (clk) is
begin
	if rising_edge(clk) then
		if rst = reset_active_g then
			ctr <= ticks_g - 1;
			clk_en_o <= '0';	
		elsif ctr = 0 then
			ctr <= ticks_g - 1;
			clk_en_o <= '1';
		else
			ctr <= ctr - 1;
			clk_en_o <= '0';
		end if;
	end if;
end process count_p;

end rtl;
