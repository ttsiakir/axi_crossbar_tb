-------------------------------------------------------------------------------
--! @file   axi_in_out.vhd
--! @author ttsiakir <theodoros.tsiakiris@cern.chr>
--! @copyright CERN SY-EPC-CCE
--! @date 17-06-2021
--! @brief  Configurable digital IO module
--! @details
--! This module provides an AXI interface to control and configure 32 IO pins.
--! The module can be configured with either an inout port with
--! configurable direction, or with separate in- and out-ports.
--! If with_builtin_tristates_g = false, DDR register does not do anything.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

--! @brief Entity declaration of axi_in_out
--! @section regmap Register memory map
--! @htmlinclude [in_out_regs.html]
entity axi_in_out is
  generic (
    --! @brief Synchronize input to local clock domain.
    --! @details Also includes a glitch filter to filter out
    --! glitches that last for max. one clock cycle.
    with_builtin_sync_g : boolean := true;
    --! @brief Generate tristates.
    --! @details If this is true, the inout port is used.
    --! If this is false, the input and output ports are used.
    with_builtin_tristates_g : boolean := true
  );
  port (
    --! @name Wishbone ports
    --! @{
    --! @name AXI clock and reset
    axi_clk_i      : in  std_logic; --! AXI clock
    axi_areset_n_i : in  std_logic; --! AXI reset signal

    --! @name AXI write address (write channel)
    axi_awvalid_i : in  std_logic; --! AXI write address valid
    axi_awready_o : out std_logic; --! AXI wrire address ready
    axi_awaddr_i  : in  std_logic_vector(3 downto 0); --! AXI write address
    axi_awprot_i  : in  std_logic_vector(2 downto 0); --! AXI write protection encoding (not used)

    --! @name AXI write data (write channel)
    axi_wvalid_i : in  std_logic; --! AXI write valid
    axi_wready_o : out std_logic; --! AXI write ready
    axi_wdata_i  : in  std_logic_vector(31 downto 0); --! AXI write data
    axi_wstrb_i  : in  std_logic_vector(3 downto 0); --!  AXI write strobe (not used)
    --
    --! @name AXI address write response (not used)
    axi_bvalid_o : out std_logic; --! AXI write response valid (not used)
    axi_bready_i : in  std_logic; --! AXI write response ready (not used)
    axi_bresp_o  : out std_logic_vector(1 downto 0); --! AXI read response (not used)

    --! @name AXI address read (write channel)
    axi_arvalid_i : in  std_logic; --! AXI read address valid
    axi_arready_o : out std_logic; --! AXI read address ready
    axi_araddr_i  : in  std_logic_vector(3 downto 0); --! AXI read address
    axi_arprot_i  : in  std_logic_vector(2 downto 0); --! AXI read address protection encoding (not used)

    --! @name AXI data read (read channel)
    axi_rvalid_o : out std_logic; --! AXI read data valid
    axi_rready_i : in  std_logic; --! AXI read data ready
    axi_rdata_o  : out std_logic_vector(31 downto 0); --! AXI read data
    axi_rresp_o  : out std_logic_vector(1 downto 0); --! AXI read response (not used)

    --! @name Digital input and output ports
    io_inout_io : inout std_logic_vector(31 downto 0) := (others => 'Z');
    io_in_i     : in    std_logic_vector(31 downto 0) := (others => '0');
    io_out_o    : out   std_logic_vector(31 downto 0) := (others => '0')

  );
end entity axi_in_out;

--! @brief Architecture definition of axi_in_out
architecture rtl of axi_in_out is

  signal DDR   : std_logic_vector(31 downto 0);
  signal PIR   : std_logic_vector(31 downto 0);
  signal POR   : std_logic_vector(31 downto 0);
  signal io_in : std_logic_vector(31 downto 0) := (others => '0');

begin

  in_out_regs : entity work.in_out_regs_axi
    port map(
      areset_n => axi_areset_n_i,
      aclk     => axi_clk_i,
      awvalid  => axi_awvalid_i,
      awready  => axi_awready_o,
      awaddr   => axi_awaddr_i,
      awprot   => axi_awprot_i,
      wvalid   => axi_wvalid_i,
      wready   => axi_wready_o,
      wdata    => axi_wdata_i,
      wstrb    => axi_wstrb_i,
      bvalid   => axi_bvalid_o,
      bready   => axi_bready_i,
      bresp    => axi_bresp_o,
      arvalid  => axi_arvalid_i,
      arready  => axi_arready_o,
      araddr   => axi_araddr_i,
      arprot   => axi_arprot_i,
      rvalid   => axi_rvalid_o,
      rready   => axi_rready_i,
      rdata    => axi_rdata_o,
      rresp    => axi_rresp_o,

      DDR_o => DDR,
      PIR_i => PIR,
      POR_o => POR
    );

  gen_with_sync: if with_builtin_sync_g = true generate
    gen_sync : entity work.external_inputs
    generic map(
      num_inputs_g => 32,
      filter_g     => true
    )
    port map(
      clk_i       => axi_clk_i,
      rst_asy_n_i => '1',
      rst_syn_i   => not axi_areset_n_i,
      sig_i       => io_in,
      sig_o       => PIR
    );
  end generate gen_with_sync;

  gen_without_sync : if with_builtin_sync_g = false generate
    PIR <= io_in;
  end generate gen_without_sync;

  gen_with_tristates : if with_builtin_tristates_g = true generate

    gpio_out_tristate : process (POR, DDR)
    begin
      for i in 0 to 31 loop
        if(DDR(i) = '1') then
          io_inout_io(i) <= POR(i);
        else
          io_inout_io(i) <= 'Z';
        end if;

      end loop;
    end process gpio_out_tristate;

    io_in <= io_inout_io;

  end generate gen_with_tristates;

  gen_without_tristates : if with_builtin_tristates_g = false generate
    io_out_o    <= POR;
    io_in       <= io_in_i;
    io_inout_io <= (others => 'Z');
  end generate gen_without_tristates;

end architecture;
