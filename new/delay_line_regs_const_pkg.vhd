package delay_line_regs_Consts is
  constant DELAY_LINE_REGS_SIZE : Natural := 16;
  constant ADDR_DELAY_LINE_REGS_CTRL : Natural := 16#0#;
  constant DELAY_LINE_REGS_CTRL_ENABLE_OFFSET : Natural := 0;
  constant DELAY_LINE_REGS_CTRL_CLEAR_OFFSET : Natural := 1;
  constant ADDR_DELAY_LINE_REGS_DLY_TICKS : Natural := 16#4#;
  constant DELAY_LINE_REGS_DLY_TICKS_PRESET : Natural := 16#0#;
  constant ADDR_DELAY_LINE_REGS_STATUS : Natural := 16#8#;
  constant DELAY_LINE_REGS_STATUS_USE_TRUE_SHIFT_REG_OFFSET : Natural := 0;
  constant DELAY_LINE_REGS_STATUS_MAX_DELAY_OFFSET : Natural := 16;
  constant ADDR_DELAY_LINE_REGS_CLOCK_FREQ : Natural := 16#c#;
end package delay_line_regs_Consts;
