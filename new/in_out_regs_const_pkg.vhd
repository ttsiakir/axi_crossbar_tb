package in_out_regs_Consts is
  constant IN_OUT_REGS_SIZE : Natural := 12;
  constant ADDR_IN_OUT_REGS_DDR : Natural := 16#0#;
  constant IN_OUT_REGS_DDR_PRESET : Natural := 16#0#;
  constant ADDR_IN_OUT_REGS_PIR : Natural := 16#4#;
  constant ADDR_IN_OUT_REGS_POR : Natural := 16#8#;
  constant IN_OUT_REGS_POR_PRESET : Natural := 16#0#;
end package in_out_regs_Consts;
