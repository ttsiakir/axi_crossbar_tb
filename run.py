from vunit import VUnit

# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()
vu.add_verification_components()

# Create library 'lib'
lib = vu.add_library("lib")
#unisims = vu.add_library("unisims")


# Add all files ending in .vhd in current working directory to library

#lib.add_source_files("/opt/Xilinx/Vivado/2019.1/data/vhdl/src/unisims/unisim_VCOMP.vhd")
#lib.add_source_files("/opt/Xilinx/Vivado/2019.1/data/vhdl/src/unisims/unisim_VPKG.vhd")

#vu.add_external_library("unisim","/opt/Xilinx/Vivado/2019.1/data/vhdl/src/unisims/")
#vu.add_external_library("unisim","/home/ansible/opt/msim_vivado_libs/unisim ")
vu.add_external_library("unisim","/opt/Xilinx/Vivado/2019.1/data/vhdl/src/unisims")

lib.add_source_files("axi_crossbar_0_sim_netlist.vhdl")
lib.add_source_files("wishbone_pkg.vhd")
lib.add_source_files("new/*.vhd")
lib.add_source_files("crossbar_tb.vhd")
#lib.add_source_files("in_out/rtl/in_out.vhd")
#lib.add_source_files("/home/ansible/opt/msim_vivado_libs/unisim/unisim_VCOMP.vhd")
#lib.add_source_files("/home/ansible/opt/msim_vivado_libs/unisim/unisim_VPKG.vhd")

# Run vunit function
vu.main()
